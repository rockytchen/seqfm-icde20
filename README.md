# README #

This is the README file for the Tensorflow implementation of SeqFM.

### HOWTO ###

(1) The model is implemented and tested with Tensorflow-GPU 1.12.0 and Numpy 1.14.0. We also have NVIDIA-SMI v430.26 and CUDA v10.0 installed.

(2) To test the model, simply run "06_SeqFM_xxx_Eval.py". We provide pre-trained parameters in the "saved_params" folder.

(3) To re-run the training process with your own data/parameter setting/tasks, modify the model details in both "6_SeqFM_xxx.py" and "06_SeqFM_xxx_Eval.py", and then run "6_SeqFM_xxx.py". 
	New parameters will be saved in the "saved_params" folder.
	To test for your own data/parameter/tasks, simply repeat step (2).
	
Note: pay attention to common errors like data format, file path etc. during your modification.

### BIBTEX ###

If you find our work helpful, please cite our paper:

@article{chen2019sequence,
 title={Sequence-Aware Factorization Machines for Temporal Predictive Analytics},
 author={Chen, Tong and Yin, Hongzhi and Nguyen, Quoc Viet Hung and Peng, Wen-Chih and Li, Xue and Zhou, Xiaofang},
 journal={ICDE},
 year={2020}
 }