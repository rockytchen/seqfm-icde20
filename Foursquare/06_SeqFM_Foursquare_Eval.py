from __future__ import print_function

import tensorflow as tf
import random
import numpy
import math

#import matplotlib.pyplot as plt
#import matplotlib
#print(matplotlib.matplotlib_fname())

import os
os.environ['CUDA_VISIBLE_DEVICES'] = '1'

def my_softmax(x):
    """Compute softmax values for each sets of scores in x."""
    return numpy.exp(x) / numpy.sum(numpy.exp(x), axis=0)

def smape(y_true, y_pred): #input type is list 
    n_samples = len(y_true) * len(y_true[0])
    y_true = numpy.reshape(numpy.array(y_true), [1, n_samples])
    y_pred = numpy.reshape(numpy.array(y_pred), [1, n_samples])
    for i in range(n_samples):
        if y_true[0, i] ==  0 and y_pred[0, i] == 0:
            y_true[0, i] = 1
            y_pred[0, i] = 1
    return numpy.mean(numpy.abs(y_true - y_pred) / (numpy.abs(y_true) + numpy.abs(y_pred))) * 200
    
def rmsle(y_true, y_pred):
    n_samples = len(y_true) * len(y_true[0])
    y_true = numpy.array(y_true)
    y_pred = numpy.array(y_pred)
    result = numpy.sqrt(numpy.sum(numpy.square(numpy.log(y_pred + 1) - numpy.log(y_true + 1))) / n_samples)
    return result

'''
raw features:
0: user_id
1: poi_id
2~6: negative samples
''' 

##############
#data handler#
##############
class DataHandler(object):

    def __init__(self, trans_file_path, neg_file_path, max_seq_len):
        '''
        initialize all data
        '''
        self.data_test = []
        self.neg_num = 0
        self.sample_count = 0
        
        print("-> loading all data now...")
        with open(trans_file_path) as in_file:
            all_trans_dic = {}
            for each_line_raw in in_file:
                each_line = each_line_raw.rstrip().split(",")
                user = int(each_line[0])
                poi = int(each_line[1])

                if user not in all_trans_dic:
                    all_trans_dic[user] = {"visited_poi": []} #store visited pois
                all_trans_dic[user]["visited_poi"].append(poi)
            '''
            new feature format:
            [ 
            line 1: [user (int), positive poi (int), poi_seq (max_len, 0 padded)]
            line 2~1001: [user (int), negtive poi (int)]
            ]
            '''
            all_neg_sample_dic = {}
            with open(neg_file_path) as neg_in_file:
                for each_line_raw in neg_in_file:
                    each_line = each_line_raw.rstrip().split(",")
                    user = int(each_line[0])
                    all_neg_sample_dic[user] = each_line[1:1001]

            for user in all_trans_dic:
                poi = all_trans_dic[user]["visited_poi"][-1] # the poi to predict
                poi_past_seq = [x + 1 for x in all_trans_dic[user]["visited_poi"][:-1]]
                
                if len(poi_past_seq) > max_seq_len:
                    this_poi_seq = poi_past_seq[-max_seq_len:]
                else:
                    this_poi_seq = [0 for j in range(max_seq_len - len(poi_past_seq))] + poi_past_seq
                feature_line = [[user, poi, this_poi_seq]]
                
                self.neg_num = len(all_neg_sample_dic[user])
                for each_neg_sample in all_neg_sample_dic[user]:
                    feature_line.append([user, each_neg_sample])
                self.data_test.append(feature_line)

        random.shuffle(self.data_test)
        self.batch_id = 0
        print("-> all data loaded.")

    def next(self):
        '''
        Return one test sample at a time.
        '''
        data_list = self.data_test
        if self.batch_id == len(data_list):
            self.batch_id = 0

        batch_feature = data_list[self.batch_id:min(self.batch_id + 1, len(data_list))]
        batch_user = []
        batch_poi = []
        batch_poi_seq = []
        for each_case in batch_feature:
            batch_user = batch_user + [[line[0]]for line in each_case]
            batch_poi = batch_poi + [[line[1]] for line in each_case]
            batch_poi_seq = batch_poi_seq + [each_case[0][2] for j in range(self.neg_num + 1)]

        self.batch_id = min(self.batch_id + 1, len(data_list))
        return batch_user, batch_poi, batch_poi_seq

'''
network parameters
'''
user_dim = 24941
poi_dim = 28593

static_len = 2
neg_size = 1000
max_len = 20
hidden_dim = 64
step_size = 0.0001

'''
two directional masks
'''
dynamic_view_mask = [[-10000.0 for j in range(max_len)] for i in range(max_len)]
for i in range(max_len):
    for j in range(max_len):
        if i >= j:
            dynamic_view_mask[i][j] = 0.0

cross_view_mask = [[-10000.0 for j in range(max_len + static_len)] for i in range(max_len + static_len)]
m = static_len - 1
for i in range(max_len):
    for j in range(max_len):
        if (i <= m and j > m) or (j <= m and i > m):
            cross_view_mask[i][j] = 0.0

'''
load and split data for training, validation and test
'''
data_handler = DataHandler("foursquare_feature_sequence.csv", "foursquare_negative_sample.csv", max_len)

print("-> loading the tf graph now...")
#tf.set_random_seed(9)

'''
running parameters
'''
display_step = 100
max_step = 500000

'''
tf graph input
'''
x_user = tf.placeholder("int32", [None, 1]) #(batch, 1)
x_poi = tf.placeholder("int32", [None, 1])
x_poi_seq = tf.placeholder("int32", [None, max_len])
dropout_keep_prob = tf.placeholder("float")

'''
define parameters (weights & biases)
'''
params = {
    #____ embedding weights ____
    "user_embedding": tf.get_variable("user_embedding", shape = [user_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "static_poi_embedding": tf.get_variable("poi_embedding", shape = [poi_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "dynamic_poi_embedding": tf.get_variable("helpful_embedding", shape = [poi_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    #____ FM weights ____    
    "user_weight": tf.get_variable("user_weight", shape = [user_dim, 1], trainable = True, initializer = tf.glorot_uniform_initializer),
    "static_poi_weight": tf.get_variable("poi_weight", shape = [poi_dim, 1], trainable = True, initializer = tf.glorot_uniform_initializer),
    "dynamic_poi_weight": tf.get_variable("helpful_weight", shape = [poi_dim, 1], trainable = True, initializer = tf.glorot_uniform_initializer),
    "global_bias": tf.get_variable("global_bias", trainable = True, initializer = tf.constant(0.0)),
    #____ network weights ____
    "projection_weight": tf.get_variable("projection_weight", shape = [3*hidden_dim, 1], trainable = True, initializer = tf.glorot_uniform_initializer), #projection matrix for regression
    "embedding_padding": tf.get_variable("embedding_padding", trainable = False, initializer = tf.constant(0.0, shape = [1, hidden_dim])), #padding line for embedding matrix
    "weight_padding": tf.get_variable("weight_padding", trainable = False, initializer = tf.constant(0.0, shape = [1, 1])), #padding line for weight
    #____ self-attention weights ____
    "Wq_static": tf.get_variable("Wq_static", shape = [1, hidden_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "Wk_static": tf.get_variable("Wk_static", shape = [1, hidden_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "Wv_static": tf.get_variable("Wv_static", shape = [1, hidden_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "Wq_dynamic": tf.get_variable("Wq_dynamic", shape = [1, hidden_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "Wk_dynamic": tf.get_variable("Wk_dynamic", shape = [1, hidden_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "Wv_dynamic": tf.get_variable("Wv_dynamic", shape = [1, hidden_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "Wq_cross": tf.get_variable("Wq_cross", shape = [1, hidden_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "Wk_cross": tf.get_variable("Wk_cross", shape = [1, hidden_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    "Wv_cross": tf.get_variable("Wv_cross", shape = [1, hidden_dim, hidden_dim], trainable = True, initializer = tf.glorot_uniform_initializer),
    #____ self-attention masks ____
    "dynamic_attn_mask": tf.get_variable("dynamic_attn_mask", trainable = False, initializer = tf.constant(dynamic_view_mask)),
    "cross_attn_mask": tf.get_variable("cross_attn_mask", trainable = False, initializer = tf.constant(cross_view_mask))}

#####################
####### SeqFM #######
#####################
def SeqFM(params, x_user, x_poi, x_poi_seq, dropout_keep_prob):
    '''
    note that due to negative sampling, the batch size is updated as: batch <- 5*batch
    '''

    '''
    embedding layer
    '''
    x_user_emb = tf.nn.embedding_lookup(params["user_embedding"], x_user) #(batch, 1, hidden)
    x_poi_emb = tf.nn.embedding_lookup(params["static_poi_embedding"], x_poi) #(batch, 1, hidden)
    this_batch_size = tf.shape(x_user_emb)[0]
    
    dynamic_poi_embedding = tf.concat([params["embedding_padding"], params["dynamic_poi_embedding"]], 0) #(poi+1, hidden)
    x_poi_seq_freq = tf.reshape(1.0/tf.cast(tf.count_nonzero(x_poi, 1, keepdims = True), tf.float32), [-1, 1, 1]) #(batch, 1, 1)
    x_poi_seq_emb = tf.nn.embedding_lookup(dynamic_poi_embedding, x_poi_seq) #(batch, maxlen, hidden)
    x_poi_seq_emb = tf.multiply(x_poi_seq_emb, tf.tile(x_poi_seq_freq, [1, max_len, hidden_dim])) #(batch, maxlen, hidden)
    
    '''
    dropout for embedding layer
    '''
    x_user_emb = tf.nn.dropout(x_user_emb, dropout_keep_prob)
    x_poi_emb = tf.nn.dropout(x_poi_emb, dropout_keep_prob)
    x_poi_seq_emb = tf.nn.dropout(x_poi_seq_emb, dropout_keep_prob)

    '''
    static view self-attention
    '''
    G_static = tf.concat([x_user_emb, x_poi_emb], 1) #(batch, 2, hidden)
    Q_static = tf.matmul(G_static, tf.tile(params["Wq_static"], [this_batch_size, 1, 1]))
    K_static = tf.matmul(G_static, tf.tile(params["Wk_static"], [this_batch_size, 1, 1]))
    V_static = tf.matmul(G_static, tf.tile(params["Wv_static"], [this_batch_size, 1, 1])) #all (batch, 2, hidden)
    A_static = tf.nn.softmax(tf.matmul(Q_static, tf.transpose(K_static, perm = [0, 2, 1]))/float(math.sqrt(hidden_dim))) #(batch, 4, 4)
    H_static = tf.matmul(A_static, V_static) #(batch, 4, hidden_dim)

    '''
    dynamic view self-attention
    '''
    G_dynamic = x_poi_seq_emb
    Q_dynamic = tf.matmul(G_dynamic, tf.tile(params["Wq_dynamic"], [this_batch_size, 1, 1]))
    K_dynamic = tf.matmul(G_dynamic, tf.tile(params["Wk_dynamic"], [this_batch_size, 1, 1]))
    V_dynamic = tf.matmul(G_dynamic, tf.tile(params["Wv_dynamic"], [this_batch_size, 1, 1])) #all (batch, maxlen, hidden)
    params_dynamic_mask = tf.tile(tf.reshape(params["dynamic_attn_mask"], [1, max_len, max_len]), [this_batch_size, 1, 1]) #(batch, maxlen, maxlen)
    masked_dot_dynamic = tf.add(tf.matmul(Q_dynamic, tf.transpose(K_dynamic, perm = [0, 2, 1]))/float(math.sqrt(hidden_dim)), params_dynamic_mask) #(batch, maxlen, maxlen)
    A_dynamic = tf.where(tf.equal(masked_dot_dynamic, 0.0), -10000.0*tf.ones_like(masked_dot_dynamic), masked_dot_dynamic) # filter out 0 values (padded items) in attn matrix
    A_dynamic = tf.nn.softmax(A_dynamic) #(batch, maxlen, maxlen)
    H_dynamic = tf.matmul(A_dynamic, V_dynamic) #(batch, maxlen, hidden)

    '''
    cross view self-attention
    '''
    G_cross = tf.concat([G_static, G_dynamic], 1) #(batch, all, hidden)
    Q_cross = tf.matmul(G_cross, tf.tile(params["Wq_cross"], [this_batch_size, 1, 1])) #(batch, all, hidden)
    K_cross = tf.matmul(G_cross, tf.tile(params["Wk_cross"], [this_batch_size, 1, 1])) #(batch, all, hidden)
    V_cross = tf.matmul(G_cross, tf.tile(params["Wv_cross"], [this_batch_size, 1, 1])) #(batch, all, hidden)
    params_cross_mask = tf.tile(tf.reshape(params["cross_attn_mask"], [1, max_len + static_len, max_len + static_len]), [this_batch_size, 1, 1]) #(batch, all, all)
    masked_dot_cross = tf.add(tf.matmul(Q_cross, tf.transpose(K_cross, perm = [0, 2, 1]))/float(math.sqrt(hidden_dim)), params_cross_mask) #(batch, all, all)
    A_cross = tf.where(tf.equal(masked_dot_cross, 0.0), -10000.0*tf.ones_like(masked_dot_cross), masked_dot_cross)
    A_cross = tf.nn.softmax(A_cross) #(batch, all, all)
    H_cross = tf.matmul(A_cross, V_cross) #(batch, all, hidden)

    '''
    intra-view pooling
    '''
    h_static = tf.reduce_sum(H_static, 1)/float(static_len)
    h_dynamic = tf.reduce_sum(H_dynamic, 1)/float(max_len)
    h_cross = tf.reduce_sum(H_cross, 1)/float(static_len + max_len) #all (batch, hidden)

    '''
    layer norm
    '''
    h_static = tf.contrib.layers.layer_norm(h_static, begin_norm_axis = 1, begin_params_axis = -1)
    h_dynamic = tf.contrib.layers.layer_norm(h_dynamic, begin_norm_axis = 1, begin_params_axis = -1)
    h_cross = tf.contrib.layers.layer_norm(h_cross, begin_norm_axis = 1, begin_params_axis = -1)        

    '''
    residual FFN layer
    '''
    h_all = tf.concat([h_static, h_dynamic, h_cross], 0) # (3*batch, hidden)
    h_ffn = tf.layers.dense(tf.contrib.layers.layer_norm(h_all, begin_norm_axis = 1, begin_params_axis = -1), units = hidden_dim, activation = tf.nn.relu, use_bias=True) #(3*batch, hidden)    
    #h_ffn = tf.layers.dense(h_ffn, units = hidden_dim, activation = None, use_bias=True) #(3*batch, hidden)
    h_ffn = tf.add(h_all, tf.nn.dropout(h_ffn, dropout_keep_prob)) #(batch, hidden)

    h_static, h_dynamic, h_cross = tf.split(h_ffn, [this_batch_size, this_batch_size, this_batch_size], 0) #each (batch, hidden)
    h_all_view = tf.concat([h_static, h_dynamic, h_cross], 1) #(batch, 3*hidden)
    ffn_output = tf.matmul(h_all_view, params["projection_weight"]) #(batch, 1)

    '''
    linear FM component
    '''
    w_user = tf.reshape(tf.nn.embedding_lookup(params["user_weight"], x_user), [-1, 1]) #(batch, 1)
    w_poi = tf.reshape(tf.nn.embedding_lookup(params["static_poi_weight"], x_poi), [-1, 1]) #(batch, 1)

    dynamic_poi_weight = tf.concat([params["weight_padding"], params["dynamic_poi_weight"]], 0) #(poi+1, 1)
    w_poi_seq = tf.multiply(tf.reduce_sum(tf.nn.embedding_lookup(dynamic_poi_weight, x_poi_seq), 1), tf.reshape(x_poi_seq_freq, [-1, 1])) #(batch, 1)     
    
    fm_output = tf.add_n([w_user, w_poi, w_poi_seq]) #(batch, 1)
    fm_output = tf.add(fm_output, params["global_bias"]) #(batch, 1)
    
    '''
    combine all outputs
    '''
    seqfm_output = tf.add(ffn_output, fm_output) #(batch, 1)

    return seqfm_output, tf.is_nan(x_user_emb)

'''
get the predictions
note that every (1+neg_size) line corresponds to a training case
'''
pred, s = SeqFM(params, x_user, x_poi, x_poi_seq, dropout_keep_prob) # ((1+negsize)*batch, 1)
pred = tf.reshape(pred, [-1, 1+neg_size]) #(batch, 1+negsize)

'''
loss and optimizer
'''
pred_pos, pred_neg = tf.split(pred, [1, neg_size], 1) #(batch, 1) and (batch, negsize)
log_loss = (0.0 - tf.reduce_sum(tf.log(pred_pos + 0.00001))) + (0.0 - tf.reduce_sum(tf.log(1.0 - pred_neg + 0.00001)))
log_loss = log_loss/tf.cast(tf.shape(pred)[0], tf.float32)
#lamda = 1.0
#log_loss = log_loss + lamda * sum(tf.nn.l2_loss(tf_var) for tf_var in tf.trainable_variables())

#optimizer = tf.train.AdamOptimizer(learning_rate = step_size).minimize(log_loss)

optimizer = tf.train.AdamOptimizer(learning_rate = step_size)
gvs = optimizer.compute_gradients(log_loss)
capped_gvs = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in gvs]
train_op = optimizer.apply_gradients(capped_gvs)

print("-> tf graph loaded.")
print("-> start training now...")

# Launch the traning session
saver = tf.train.Saver()
with tf.Session() as sess:
    #initialize all parameters before the training loop starts
    saver.restore(sess, "saved_params/SeqFM/epoch_30.ckpt")
    print("-> model restored.")

    all_hits = {5:0.0, 10:0.0, 20:0.0}
    all_ndcg = {5:0.0, 10:0.0, 20:0.0}
    while data_handler.batch_id < user_dim:
        batch_user, batch_poi, batch_poi_seq = data_handler.next()
        
        score_pos, score_neg = sess.run([pred_pos, pred_neg], feed_dict={x_user: batch_user,
                                                                          x_poi: batch_poi, 
                                                                      x_poi_seq: batch_poi_seq,
                                                              dropout_keep_prob: 1.0})
                                       
        score_pos = score_pos[0,0]
        score_neg = numpy.fliplr(numpy.sort(score_neg))
        #print(score_pos)
        #print(score_neg)

        if score_pos >= score_neg[0,19]:
            all_hits[20] += 1.0
            if score_pos >= score_neg[0,9]:
                all_hits[10] += 1.0
                if score_pos >= score_neg[0,4]:
                    all_hits[5] += 1.0
        
        dcg = 0.0
        for i in range(20):
            if score_pos >= score_neg[0,i]:
                ndcg = 1.0/math.log2(1 + float(i+1))
                score_pos = -10000.0
                if i <= 19:
                    all_ndcg[20] += ndcg
                    if i <= 9:
                        all_ndcg[10] += ndcg
                        if i <= 4:
                            all_ndcg[5] += ndcg
                
        if data_handler.batch_id % 100 == 0:
            hit5_avg = all_hits[5]/float(data_handler.batch_id)
            hit10_avg = all_hits[10]/float(data_handler.batch_id)
            hit20_avg = all_hits[20]/float(data_handler.batch_id)

            ndcg5_avg = all_ndcg[5]/float(data_handler.batch_id)
            ndcg10_avg = all_ndcg[10]/float(data_handler.batch_id)
            ndcg20_avg = all_ndcg[20]/float(data_handler.batch_id)
            print("-> tested samples: " + str(data_handler.batch_id) + 
                  ", hit@5, 10, 20: " + "{:.3f}".format(hit5_avg) + "," + "{:.3f}".format(hit10_avg) + "," + "{:.3f}".format(hit20_avg) +
                  ", ndcg@5, 10, 20: " + "{:.3f}".format(ndcg5_avg) + "," + "{:.3f}".format(ndcg10_avg) + "," + "{:.3f}".format(ndcg20_avg))

    hit5_avg = all_hits[5]/float(user_dim)
    hit10_avg = all_hits[10]/float(user_dim)
    hit20_avg = all_hits[20]/float(user_dim)

    ndcg5_avg = all_ndcg[5]/float(user_dim)
    ndcg10_avg = all_ndcg[10]/float(user_dim)
    ndcg20_avg = all_ndcg[20]/float(user_dim)
    print("\n-> test finished" + 
          ", hit@5, 10, 20: " + "{:.3f}".format(hit5_avg) + "," + "{:.3f}".format(hit10_avg) + "," + "{:.3f}".format(hit20_avg) +
          ", ndcg@5, 10, 20: " + "{:.3f}".format(ndcg5_avg) + "," + "{:.3f}".format(ndcg10_avg) + "," + "{:.3f}".format(ndcg20_avg))

    print("-> trainig finished.")












